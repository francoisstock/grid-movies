import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Actor } from "../shared/actor.model";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "mov-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"]
})
export class ListComponent implements OnInit {
  public actors$: Observable<Actor[]>;

  constructor(private readonly client: HttpClient) {}

  ngOnInit() {
    this.actors$ = this.client.get<Actor[]>(
      "https://localhost:5001/api/actors",
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      }
    );
  }
}
