export class Actor {
  id: string;

  firstName: string;
  lastName: string;
  birthDate: Date;
  isAlive: boolean;
  popularity: number;
  pictureUri: string;

  egotDisplay: string;
}
