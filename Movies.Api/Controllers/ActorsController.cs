using System;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Data;
using Movies.Api.Domain;
using Movies.Api.Model;

namespace Movies.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ActorsController : ControllerBase
    {
        public ActorsController()
        {
            Repository = new ActorRepository();
        }

        private ActorRepository Repository { get; }

        [HttpGet("")]
        public ActionResult<PaginatedResult<Actor>> GetAll(
            [FromQuery]ActorFilter filter,
            [FromQuery] Pagination pagination,
            [FromQuery]string orderBy)
        {
            var predicate = new ActorPredicate(filter, pagination, orderBy);
            var data = Repository.GetAll(predicate);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public ActionResult<Actor> GetOne([FromRoute]Guid id)
        {
            var data = Repository.GetOne(id);
            return Ok(data);
        }
    }
}