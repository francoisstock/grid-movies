using System;

namespace Movies.Api.Domain
{
    public class Actor
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public bool IsAlive { get; set; }

        public double Popularity { get; set; }

        public string PictureUri { get; set; }

        public Egot Egot { get; set; }
        public string EgotDisplay => Egot.ToString();
    }
}