using System;
namespace Movies.Api.Domain
{
    [Flags]
    public enum Egot : short
    {
        None,
        Emmy = 1,
        Grammy = 2,
        Oscar = 4,
        Tony = 8
    }
}