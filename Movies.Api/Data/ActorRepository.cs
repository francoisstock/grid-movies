using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Movies.Api.Domain;
using System;
using System.IO;
using Newtonsoft.Json;
using Movies.Api.Model;
using System.Linq;

namespace Movies.Api.Data
{
    public class ActorRepository
    {
        public readonly IQueryable<Actor> _actors;

        public ActorRepository()
        {
            _actors = GetData();
        }

        private static IQueryable<Actor> GetData()
        {
            // Cf. https://next.json-generator.com for data generation
            var json = File.ReadAllText("Data/Actors.json");
            var result = JsonConvert.DeserializeObject<List<Actor>>(json);

            return result.AsQueryable();
        }

        public PaginatedResult<Actor> GetAll(ActorPredicate predicate)
        {
            return _actors
                .FilterBy(predicate.Filter)
                .OrderBy(predicate.OrderBy)
                .ToPaginatedResult(predicate.Pagination);
        }

        public Actor GetOne(Guid id) => _actors.SingleOrDefault(a => a.Id == id);
    }
}