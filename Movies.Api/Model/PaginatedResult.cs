using System.Collections.Generic;
using System.Linq;

namespace Movies.Api.Model
{
    public class PaginatedResult<TEntity>
    {
        public IReadOnlyList<TEntity> Data { get; set; }

        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Total { get; set; }
    }

    public static class PaginatedResultExtensions
    {
        public static PaginatedResult<TEntity> ToPaginatedResult<TEntity>(
            this IQueryable<TEntity> source, Pagination pagination
            )
        {
            var skip = (pagination.Page - 1) * pagination.PageSize;
            var take = pagination.PageSize;

            return new PaginatedResult<TEntity>
            {
                Data = source.Skip(skip).Take(take).ToList(),

                Page = pagination.Page,
                PageSize = pagination.PageSize,
                Total = source.Count()
            };
        }

        // public static PaginatedResult<TEntity> ToPaginatedResult<TEntity>(
        //     this IEnumerable<TEntity> source, Pagination pagination
        // ) => source.AsQueryable().ToPaginatedResult(pagination);
    }
}