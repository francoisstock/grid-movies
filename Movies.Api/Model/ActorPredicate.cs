using System.Linq;
namespace Movies.Api.Model
{
    public class ActorPredicate
    {
        private static readonly string[] AvailableOrderBy = new[] {
            "firstName","lastName", "birthDate", "popularity"
        };

        public ActorPredicate(ActorFilter filter, Pagination pagination, string orderBy)
        {
            Filter = filter ?? new ActorFilter();
            Pagination = pagination ?? new Pagination();
            OrderBy = SanitizeOrderBy(orderBy);
        }

        private static string SanitizeOrderBy(string orderBy)
        {
            var orderBys = (orderBy ?? "").Split(',');
            var sanitizedOrderBys = orderBys.Where(IsAvailable).ToArray();
            return sanitizedOrderBys.Any() ? GetOrderByAggreggate(sanitizedOrderBys) : "birthDate desc";
        }

        private static string GetOrderByAggreggate(string[] orderBys) => orderBys.Aggregate((acc, value) => acc += $", {value}");

        private static bool IsAvailable(string orderBy) =>
            ActorPredicate.AvailableOrderBy.Any(o => o == orderBy.Replace(" desc", ""));

        public ActorFilter Filter { get; }

        public Pagination Pagination { get; set; }

        public string OrderBy { get; }
    }
}