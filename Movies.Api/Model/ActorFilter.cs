using System.Linq.Expressions;
using System.Linq;
using System;
using Movies.Api.Domain;

namespace Movies.Api.Model
{
    public class ActorFilter
    {
        public string Search { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public bool? IsAlive { get; set; }

        // public double? Popularity { get; set; }

        public int? Egot { get; set; }
    }

    public static class ActorFilterExtensions
    {
        public static IQueryable<Actor> FilterBy(this IQueryable<Actor> source, ActorFilter filter)
        {
            // var foo = ((int)Egot.Emmy & filter.Egot.Value);

            return source
                .Where(SearchByFirstName(filter.FirstName))
                .Where(SearchByLastName(filter.LastName))
                .Where(SearchByBirthDate(filter.BirthDate))
                .Where(SearchByIsAlive(filter.IsAlive))
                .Where(SearchByEgot(filter.Egot));
        }

        private static Expression<Func<Actor, bool>> SearchByFirstName(string firstName)
        {
            return (actor => string.IsNullOrWhiteSpace(firstName) || actor.FirstName.Contains(firstName));
        }

        private static Expression<Func<Actor, bool>> SearchByLastName(string lastName)
        {
            return (actor => string.IsNullOrWhiteSpace(lastName) || actor.LastName.Contains(lastName));
        }

        private static Expression<Func<Actor, bool>> SearchByBirthDate(DateTime? birthDate)
        {
            return (actor => !birthDate.HasValue || actor.BirthDate == birthDate.Value);
        }

        private static Expression<Func<Actor, bool>> SearchByIsAlive(bool? isAlive)
        {
            return (actor => !isAlive.HasValue || actor.IsAlive == isAlive.Value);
        }

        private static Expression<Func<Actor, bool>> SearchByEgot(int? egot)
        {
            // TODO: fix
            return (actor => !egot.HasValue || ((int)actor.Egot & egot.Value) == (int)actor.Egot);
        }
    }
}